//
// Created by Konrad Buchmann on 2019-02-22.
//

#pragma once

#include <cstdint>


struct Color {
    uint8_t r, g, b;

    Color() = default;
    Color(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {}

    static const Color red;
    static const Color green;
    static const Color blue;
    static const Color black;
    static const Color white;

};

const Color Color::red   = Color(255,0,0);
const Color Color::green = Color(0,255,0);
const Color Color::blue  = Color(0,0,255);
const Color Color::black = Color(0,0,0);
const Color Color::white = Color(255,255,255);
