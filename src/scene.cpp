//
// Created by Konrad Buchmann on 2019-03-03.
//

#include <optional>
#include "scene.hpp"
#include "hitrecord.hpp"
#include "ray.hpp"

std::optional<HitRecord> Scene::hit(const Ray &r, float t_min, float t_max) const {
	std::optional<HitRecord> closest_hit_record = std::nullopt;
	float closest_hit_t = MAXFLOAT;
	
	for (const auto &object : objects) {
		auto hit_record = object->hit(r, t_min, t_max);
		if (hit_record.has_value()) {
			if (hit_record->t < closest_hit_t) {
				closest_hit_record = hit_record;
				closest_hit_t = hit_record->t;
			}
		}
	}
	
	return closest_hit_record;
}