//
// Created by Konrad Buchmann on 2019-03-03.
//

#include "sphere.hpp"
#include <Eigen/Dense>

using Eigen::Vector3f;



std::optional<float> Sphere::intersect_sphere(const Ray &r) const {
	auto oc = r.origin - center;
	float a = r.direction.dot(r.direction);
	float b = 2.f * r.direction.dot(oc);
	float c = oc.dot(oc) - radius * radius;
	float d = b*b - 4.f*a*c;
	if (d < 0.f) {
		return std::nullopt;
	} else {
		return std::optional<float>((-b - sqrt(d)) / (2.f * a));
	}
}


std::optional<HitRecord> Sphere::hit(const Ray &r, float t_min, float t_max) const {
	auto oc = r.origin - center;
	auto a = r.direction.dot(r.direction);
	auto b = r.direction.dot(oc);
	auto c = oc.dot(oc) - radius * radius;
	auto d = b*b - a*c;
	
	if (b*b - a*c > 0) {
		HitRecord hitRecord;
		
		//TODO: code duplication
		auto t = (-b - sqrt(b*b - a*c)) / a;
		if (t_min < t && t < t_max) {
			hitRecord.t = t;
			hitRecord.p = r.point_at(t);
			hitRecord.normal = (hitRecord.p - center) / radius;
			return hitRecord;
		}
		
		t = (-b + sqrt(b*b - a*c)) / a;
		if (t_min < t && t < t_max) {
			hitRecord.t = t;
			hitRecord.p = r.point_at(t);
			hitRecord.normal = (hitRecord.p - center) / radius;
			return hitRecord;
		}
		
	}
	
	return std::nullopt;
	
}