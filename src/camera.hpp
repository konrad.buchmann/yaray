//
// Created by Konrad Buchmann on 2019-03-11.
//

#pragma once

#include <Eigen/Dense>
#include "ray.hpp"

using Eigen::Vector3f;

class Camera {
private:
	Vector3f origin;
	Vector3f screen_origin;
	Vector3f screen_u;
	Vector3f screen_v;
	
public:
	Camera() :
		origin(Vector3f(0,0,0)),
		screen_origin(Vector3f(-2,-1,-2)),
		screen_u(Vector3f(4,0,0)),
		screen_v(Vector3f(0,2,0))
	{}
	
	Ray get_ray(float u, float v) {
		return Ray(origin, screen_origin + u * screen_u + v * screen_v);
	}
};