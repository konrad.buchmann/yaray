#pragma once

#include <optional>
#include "ray.hpp"
#include "hitrecord.hpp"


class Hitable {
public:
	virtual std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) const = 0;
	virtual ~Hitable() = default;
};