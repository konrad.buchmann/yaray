//
// Created by Konrad Buchmann on 2019-03-03.
//

#pragma once


#include <vector>
#include <memory>
#include "hitable.hpp"

class Scene : public Hitable{
private:
	std::vector<std::unique_ptr<Hitable>> objects;
	
public:
	template<typename T>
	void add_object(T object) {
		objects.push_back(std::make_unique<T>(object));
	}
	
	std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) const override;
	
};