#pragma once

#include "hitable.hpp"
#include <utility>
#include <Eigen/Dense>
using Eigen::Vector3f;


class Sphere : public Hitable{
private:
	Vector3f center;
	float radius;
	
	std::optional<float> intersect_sphere(const Ray &r) const;
	
public:
	Sphere(Vector3f center, float radius) : center(std::move(center)), radius(radius) {}
	
	std::optional<HitRecord> hit(const Ray &r, float t_min, float t_max) const override;
	
};