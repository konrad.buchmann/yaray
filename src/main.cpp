#include <iostream>
#include <png++/png.hpp>
#include <Eigen/Dense>
#include <random>
#include "ray.hpp"
#include "color.hpp"
#include "hitable.hpp"
#include "sphere.hpp"
#include "scene.hpp"
#include "camera.hpp"

using Eigen::Vector3f;

std::default_random_engine random_engine;
std::uniform_real_distribution<float> distrib{0.0, 1.0};

Vector3f get_background_color(const Ray &r) {
	Vector3f view_normal = (r.direction - r.origin);
	float b = 0.5f * (view_normal.y() + 1.f);
	return Vector3f{(1 - b) * 128 + b * 255, (1 - b) * 178 + b * 255, 255} / 256;
}

Color normal2color(const Vector3f &v) {
	return Color( (v.x() + 1) * 127, (v.y() + 1) * 127, (v.z() + 1) * 127 );
}

Vector3f random_in_unit_sphere() {
    Vector3f p;
    do {
        p = 2.f * Vector3f{distrib(random_engine), distrib(random_engine), distrib(random_engine)} - Vector3f{1,1,1};
    } while (p.squaredNorm() >= 1.0);
    return p;
}

Vector3f get_color(const Ray &ray_in, const Scene &scene) {
    auto hit_record = scene.hit(ray_in, 0.f, MAXFLOAT);
    
    if (hit_record.has_value()) {
        Vector3f p_out = hit_record->p + hit_record->normal + random_in_unit_sphere();
        return 0.5 * get_color(Ray{hit_record->p, p_out - hit_record->p}, scene);
    } else {
        return get_background_color(ray_in);
    }
}

int main() {
    
    const int samples = 64;
    
    png::image<png::rgb_pixel> image(800, 400);
    
	Scene scene {};
	scene.add_object(Sphere{Vector3f{0, 0, -2}, 0.5});
	scene.add_object(Sphere{Vector3f{0.5f, 0, -1.5f}, 0.2});
	scene.add_object(Sphere{Vector3f{0, -100.5f, -2}, 100});
	
	Camera camera {};
	
	for (int i = 0; i < image.get_width(); ++i) {
		for (int j = 0; j < image.get_height(); ++j) {
		
		    Vector3f c{0,0,0};
		    for (int s = 0; s < samples; ++s) {
	            float u = (i + distrib(random_engine)) / image.get_width();
	            float v = (j + distrib(random_engine)) / image.get_height();
                
                auto camera_ray = camera.get_ray(u, v);
                
                c += get_color(camera_ray, scene);
            }
            
            c /= samples;
            png::rgb_pixel aa_color{static_cast<unsigned char>(c.x() * 255),
                                    static_cast<unsigned char>(c.y() * 255),
                                    static_cast<unsigned char>(c.z() * 255)
            };
            
            image.set_pixel(i, image.get_height() - (j + 1), aa_color);

        }
    }

    image.write("rgb.png");

}