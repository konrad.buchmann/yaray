#include <utility>

#include <utility>

#pragma once

#include <Eigen/Dense>
using Eigen::Vector3f;


class Ray {
public:
	const Vector3f origin;
	const Vector3f direction;
	
	Ray() = default;
	Ray(Vector3f origin, Vector3f direction) : origin(std::move(origin)), direction(std::move(direction)) {}
	
	auto point_at(float t) const {
		return origin + t * direction;
	}
};