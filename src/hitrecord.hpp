
#pragma once

#include <Eigen/Dense>
using Eigen::Vector3f;


struct HitRecord {
	float t;
	Vector3f p;
	Vector3f normal;
};